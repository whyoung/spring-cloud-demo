package why.demo.springcloud.utils;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TimeUtils {

    private static final Random RANDOM = new Random();

    public static void sleep(int seconds) {
        int bound = (int) TimeUnit.SECONDS.toMillis(seconds);
        try {
            TimeUnit.MILLISECONDS.sleep(RANDOM.nextInt(bound));
        } catch (InterruptedException ignore) {
        }
    }
}
