package why.demo.springcloud.calc.impl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import why.demo.springcloud.calc.api.Calculator;
import why.demo.springcloud.utils.TimeUtils;

@RestController
public class CalcController implements Calculator {

    @Override
    @RequestMapping("/add")
    public int add(@RequestParam int a, @RequestParam int b) {
        TimeUtils.sleep(2);
        return a + b;
    }
}
