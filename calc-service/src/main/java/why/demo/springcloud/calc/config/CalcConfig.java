package why.demo.springcloud.calc.config;

import brave.sampler.Sampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CalcConfig {

    @Bean
    public Sampler newSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}
