package why.demo.springcloud.test.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import why.demo.springcloud.calc.api.Calculator;
import why.demo.springcloud.generator.api.IdGenerator;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    private RestTemplate template;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Autowired
    private Calculator calcApi;

    @Autowired
    private IdGenerator generator;

    @RequestMapping("/add")
    @ResponseBody
    public Integer add(@RequestParam int a, @RequestParam int b) {
        ServiceInstance instance = loadBalancerClient.choose("CALC-SERVICE");
        System.out.println(instance);
        Map<String, Object> params = new HashMap<>();
        params.put("a", a);
        params.put("b", b);
        return template.getForObject("http://CALC-SERVICE/add?a={a}&b={b}", int.class, params);
    }

    @RequestMapping("/ribbon")
    public void ribbon() {
        ServiceInstance instance = loadBalancerClient.choose("CALC-SERVICE");
        System.out.println(instance);
    }

    @RequestMapping("/add2")
    @ResponseBody
    public Integer add2(@RequestParam int a, @RequestParam int b) {
        return calcApi.add(a, b);
    }


    @RequestMapping("/ids")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "emptyIds")
    public List<Long> ids(@RequestParam int count) {
        return generator.ids(count);
    }

    public List<Long> emptyIds(int count) {
        return Collections.emptyList();
    }

    @RequestMapping("/info")
    public void info(HttpServletRequest request) {
        System.out.println(request.getHeader("auth"));
    }
}
