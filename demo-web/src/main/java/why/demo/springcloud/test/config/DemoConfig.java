package why.demo.springcloud.test.config;

import brave.sampler.Sampler;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DemoConfig {

    /**
     * LoadBalanced 注解通过给 RestTemplate增加拦截器的方式，让RestTemplate具备负载均衡的能力
     */
    @Bean
    @LoadBalanced
    public RestTemplate template() {
        return new RestTemplate();
    }

    //负载均衡规则
    @Bean
    public IRule roundRobinRule() {
        return new RoundRobinRule();
    }

    //sleuth 采样规则
    @Bean
    public Sampler newSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}
