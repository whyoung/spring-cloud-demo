package why.demo.springcloud.generator.controller;

import org.springframework.web.bind.annotation.*;
import why.demo.springcloud.generator.api.IdGenerator;
import why.demo.springcloud.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class IdGeneratorController implements IdGenerator {

    private final AtomicLong generator = new AtomicLong(0);

    @Override
    @RequestMapping("/id")
    public Long id() {
        return generator.incrementAndGet();
    }

    @Override
    @RequestMapping("/ids")
    @ResponseBody
    public List<Long> ids(@RequestParam("count") int count) {
        if (count <= 0) {
            throw new IllegalArgumentException("size must be positive");
        }
        TimeUtils.sleep(2);
        List<Long> list = new ArrayList<>(count);
        for (int i = 0; i<count; i++) {
            list.add(generator.incrementAndGet());
        }
        return list;
    }
}
