package why.demo.springcloud.generator.config;

import brave.sampler.Sampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdGeneratorConfig {

    @Bean
    public Sampler newSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}
