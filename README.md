# spring-cloud-demo

> spring cloud demo

模块

- demo-web              对外提供接口入口
- calc-api              接口定义
- calc-service          接口实现
- id—generator-api      接口定义
- id-generator-service  接口实现
- zuul                  网关
- config                spring cloud config

需要的组件 [zipkin](https://zipkin.io)