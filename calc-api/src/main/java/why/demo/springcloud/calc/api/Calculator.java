package why.demo.springcloud.calc.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import why.demo.springcloud.calc.config.CalcFeignConfig;

@FeignClient(value = "calc-service", configuration = CalcFeignConfig.class)
public interface Calculator {

    @RequestMapping("/add")
    int add(@RequestParam(name = "a") int a, @RequestParam(name = "b") int b);
}
