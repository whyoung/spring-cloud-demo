package why.demo.springcloud.generator.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import why.demo.springcloud.generator.config.IdGeneratorFeignConfig;

import java.util.List;

@FeignClient(value = "id-generator-service", configuration = IdGeneratorFeignConfig.class)
public interface IdGenerator {

    @RequestMapping("/id")
    Long id();

    @RequestMapping("/ids")
    @ResponseBody
    List<Long> ids(@RequestParam("count") int count);
}
