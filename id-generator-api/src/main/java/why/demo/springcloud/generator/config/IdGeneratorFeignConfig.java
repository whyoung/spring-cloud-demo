package why.demo.springcloud.generator.config;

import feign.Logger;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class IdGeneratorFeignConfig {

    //重试机制
    @Bean("idGeneratorServiceRetryer")
    public Retryer newRetryer() {
        return new Retryer.Default(100, TimeUnit.SECONDS.toMillis(2), 5);
    }

    @Bean("idGeneratorFeignLogLevel")
    public Logger.Level calcFeignLogLevel() {
        return Logger.Level.FULL;
    }
}
